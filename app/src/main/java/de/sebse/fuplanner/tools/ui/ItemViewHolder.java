package de.sebse.fuplanner.tools.ui;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import de.sebse.fuplanner.R;

public class ItemViewHolder extends CustomViewHolder {
    public final TextView mTitle;
    public final TextView mSubLeft;
    public final TextView mSubRight;
    private final TextView mTopRight;

    public ItemViewHolder(View view) {
        super(view);
        mTitle = view.findViewById(R.id.title);
        mSubLeft = view.findViewById(R.id.sub_left);
        mSubRight = view.findViewById(R.id.sub_right);
        mTopRight = view.findViewById(R.id.top_right);
    }

    @NonNull
    @Override
    public String toString() {
        return super.toString() + " '" + mTitle.getText() + "' '" + mSubLeft.getText() + "' '" + mSubRight.getText() + "' '" + mTopRight.getText() + "'";
    }
}
