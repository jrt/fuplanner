package de.sebse.fuplanner.tools.network;

public interface NetworkErrorCallback<T> {
    void onError(NetworkError error);
}
