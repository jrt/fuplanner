package de.sebse.fuplanner.services.kvv.sync;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class KVVContentProvider extends ContentProvider {

    public static final String PROVIDER_NAME = "de.sebse.fuplanner.contentprovider.kvv.modules";
    private static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/modules");
    private static final int MODULE = 1;
    private static final int MODULE_ID = 2;
    private static final UriMatcher uriMatcher = getUriMatcher();

    private static UriMatcher getUriMatcher() {
        UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "modules", MODULE);
        uriMatcher.addURI(PROVIDER_NAME, "modules/#", MODULE_ID);
        return uriMatcher;
    }


    @Override
    public boolean onCreate() {
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] strings, @Nullable String s, @Nullable String[] strings1, @Nullable String s1) {
        return null;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)) {
            case MODULE:
                return "vnd.android.cursor.dir/vnd.com."+PROVIDER_NAME;
            case MODULE_ID:
                return "vnd.android.cursor.item/vnd.com."+PROVIDER_NAME;

        }
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }
}
