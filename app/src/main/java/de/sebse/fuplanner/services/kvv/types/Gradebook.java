package de.sebse.fuplanner.services.kvv.types;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Gradebook implements Serializable, Iterable<Grade> {
    private ArrayList<Grade> gradebook = new ArrayList<>();
    private final static String[] EXAM_KEYWORDS = new String[] {"exam", "klausur", "prüfung"};
    private final static String[] ASSIGNMENT_KEYWORDS = new String[] {"übung", "assignment", "blatt", "sheet", "exercise"};

    public void addGrade(Grade grade) {
        gradebook.add(0, grade);
    }

    public CategorizedGrades getAutoCategorizedGrades() {
        ArrayList<Grade> exams = new ArrayList<>();
        ArrayList<Grade> assignments = new ArrayList<>();
        ArrayList<Grade> others = new ArrayList<>();
        for (Grade grade : gradebook) {
            boolean found = false;
            String name = grade.getItemName().toLowerCase();
            for (String keyword : EXAM_KEYWORDS) {
                if (name.contains(keyword)) {
                    exams.add(grade);
                    found = true;
                    break;
                }
            }
            if (found) continue;
            for (String keyword : ASSIGNMENT_KEYWORDS) {
                if (name.contains(keyword)) {
                    assignments.add(grade);
                    found = true;
                    break;
                }
            }
            if (found) continue;
            others.add(grade);
        }
        return new CategorizedGrades(exams, assignments, others);
    }

    public GradebookOutput getUncategorizedGrades() {
        return new GradebookOutput(gradebook);
    }

    @NonNull
    @Override
    public Iterator<Grade> iterator() {
        return gradebook.iterator();
    }

    public class CategorizedGrades {
        private GradebookOutput exams;
        private GradebookOutput assignments;
        private GradebookOutput others;

        private CategorizedGrades(List<Grade> exams, List<Grade> assignments, List<Grade> others) {
            this.exams = new GradebookOutput(exams);
            this.assignments = new GradebookOutput(assignments);
            this.others = new GradebookOutput(others);
        }

        public GradebookOutput getExams() {
            return exams;
        }

        public GradebookOutput getAssignments() {
            return assignments;
        }

        public GradebookOutput getOthers() {
            return others;
        }
    }

    public class GradebookOutput {
        public List<Grade> grades;

        private GradebookOutput(List<Grade> grades) {
            this.grades = Collections.unmodifiableList(grades);
        }

        public float getPercentage() {
            float maxPoints = 0;
            float userPoints = 0;
            if (grades != null) {
                for (Grade g : grades){
                    maxPoints += g.getMaxPoints();
                    userPoints += g.getPoints();
                }
            }
            if (maxPoints == 0)
                return 0;
            return userPoints / maxPoints;
        }

        public float getMaxPointSum() {
            float maxPoints = 0;
            if (grades != null) {
                for (Grade g : grades){
                    maxPoints += g.getMaxPoints();
                }
            }
            return maxPoints;
        }

        public float getUserPointSum() {
            float userPoints = 0;
            if (grades != null) {
                for (Grade g : grades){
                    userPoints += g.getPoints();
                }
            }
            return userPoints;
        }

        public Grade getBestGrade() {
            Grade bestGrade = null;
            double bestValue = -1;
            if (grades != null) {
                for (Grade g : grades){
                    double perc = g.getPoints() / g.getMaxPoints();
                    if (perc > bestValue) {
                        bestValue = perc;
                        bestGrade = g;
                    }
                }
            }
            return bestGrade;
        }
    }
}
