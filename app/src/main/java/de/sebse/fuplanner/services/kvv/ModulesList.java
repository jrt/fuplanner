package de.sebse.fuplanner.services.kvv;

import android.content.Context;

import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;

import androidx.arch.core.util.Function;
import de.sebse.fuplanner.services.kvv.types.CacheBBCourse;
import de.sebse.fuplanner.services.kvv.types.CacheKVVCourse;
import de.sebse.fuplanner.services.kvv.types.Lecturer;
import de.sebse.fuplanner.services.kvv.types.Modules;
import de.sebse.fuplanner.services.kvv.types.Semester;
import de.sebse.fuplanner.tools.NewAsyncQueue;
import de.sebse.fuplanner.tools.Regex;
import de.sebse.fuplanner.tools.network.HTTPService;
import de.sebse.fuplanner.tools.network.NetworkCallback;
import de.sebse.fuplanner.tools.network.NetworkError;
import de.sebse.fuplanner.tools.network.NetworkErrorCallback;

import static de.sebse.fuplanner.services.kvv.Part.RETRY_COUNT;

public class ModulesList extends HTTPService {
    private final Login mLogin;
    private final KVVListener mListener;
    @Nullable private Modules mModules;
    private ModulesListLecturer mLecturer;
    private CacheBBCourse mBBCache;
    private final NewAsyncQueue mQueue = new NewAsyncQueue();
    private CacheKVVCourse mKVVCache;

    ModulesList(Login login, KVVListener listener, Context context) {
        super(context);
        this.mLogin = login;
        this.mListener = listener;
        restore();
    }

    @Nullable
    public String getUsername() {
        if (mModules != null) {
            return mModules.getUsername();
        }
        return null;
    }

    public void reloadIfOutdated() {
        try {
            if (mModules != null && mModules.isNewerVersionInStorage(getContext())) {
                restore();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void find(String moduleID, NetworkCallback<Modules.Module> moduleNetworkCallback, NetworkErrorCallback errorCallback) {
        find(moduleID, moduleNetworkCallback, errorCallback, RETRY_COUNT);
    }

    private void find(String moduleID, NetworkCallback<Modules.Module> moduleNetworkCallback, NetworkErrorCallback errorCallback, int retries) {
        if (mModules != null &&
                mLogin.getLoginTokenKVV() != null && mLogin.getLoginTokenKVV().isOtherUser(mModules.getUsername()) &&
                mLogin.getLoginTokenBB() != null && mLogin.getLoginTokenBB().isOtherUser(mModules.getUsername())
        )
            delete();
        if (retries < 0) {
            log.t("Too many retires", retries);
            errorCallback.onError(new NetworkError(101107, -1, "Too many retries!"));
            return;
        }
        if (this.mModules != null) {
            Modules.Module module = this.mModules.get(moduleID);
            if (module != null) {
                moduleNetworkCallback.onResponse(module);
                return;
            }
        }
        recv(success -> find(moduleID, moduleNetworkCallback, errorCallback, retries - 1), errorCallback, true, RETRY_COUNT);
    }

    void store() {
        if (this.mModules != null) {
            try {
                this.mModules.save(getContext());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void restore() {
        try {
            this.mModules = Modules.load(getContext());
        } catch (FileNotFoundException ignored) {
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            delete();
        }
        if (this.mModules == null) {
            recv(success -> {}, log::e, true);
        }
    }

    public void delete() {
        if (this.mModules != null) {
            this.mModules.delete(getContext());
            this.mModules = null;
        }
    }

    public void recv(final NetworkCallback<Modules> callback, final NetworkErrorCallback errorCallback) {
        recv(callback, errorCallback, false);
    }

    public void recv(final NetworkCallback<Modules> callback, final NetworkErrorCallback errorCallback, boolean forceRefresh) {
        recv(callback, errorCallback, forceRefresh, RETRY_COUNT);
    }

    private void recv(final NetworkCallback<Modules> callback, final NetworkErrorCallback errorCallback, boolean forceRefresh, final int retries) {
        if (mModules != null &&
                mLogin.getLoginTokenKVV() != null && mLogin.getLoginTokenKVV().isOtherUser(mModules.getUsername()) &&
                mLogin.getLoginTokenBB() != null && mLogin.getLoginTokenBB().isOtherUser(mModules.getUsername())
        )
            delete();
        if (this.mModules != null && !forceRefresh) {
            callback.onResponse(this.mModules);
            return;
        }
        mQueue.add(() -> {
            if (mLogin.isLoginPending()) {
                mLogin.restoreOnlineLogin(resCode -> {
                    mQueue.next();
                });
            } else {
                mQueue.next();
            }
        });
        mQueue.add(() -> {
            Function<Integer, NetworkErrorCallback> errorFunc = ((Integer errorCode) -> (error -> {
                if (retries > 0 && (error.getHttpStatus() == 401 || error.getHttpStatus() == 403)) {
                    mLogin.refreshLogin(success -> {
                        recv(callback, errorCallback, forceRefresh, retries - 1);
                        mQueue.next();
                    }, error1 -> {
                        errorCallback.onError(error1);
                        mQueue.next();
                    }, errorCode);
                    return;
                }
                errorCallback.onError(error);
                mQueue.next();
            }));
            this.upgradeKVV(successKVV -> {
                this.upgradeBB(successKVV, success -> {
                    if (this.mModules == null)
                        this.mModules = success;
                    else if (this.mModules.updateList(success)) {
                        mListener.onModuleListChange();
                        store();
                    }
                    callback.onResponse(this.mModules);
                    mQueue.next();
                }, errorFunc.apply(Login.LOGOUT_BB));
            }, errorFunc.apply(Login.LOGOUT_KVV));
        });
    }

    private void upgradeKVV(final NetworkCallback<Modules> callback, final NetworkErrorCallback errorCallback) {
        NetworkCallback<Modules> successCallback = (modules -> {
            try {
                cacheKVVCourse().save(getContext());
            } catch (IOException e) {
                e.printStackTrace();
            }
            callback.onResponse(modules);
        });

        if (!mLogin.isInOnlineMode() || mLogin.getLoginTokenKVV() == null) {
            errorCallback.onError(new NetworkError(101110, 500, "Currently running in offline mode!"));
            return;
        }
        Modules modules = new Modules(mLogin.getLoginTokenKVV().getUsername());
        if (!mLogin.getLoginTokenKVV().isAvailable()) {
            callback.onResponse(modules);
            return;
        }
        get(Constants.KVV_SERVER_URL+"direct/membership.json?_validateSession=", mLogin.getLoginTokenKVV().getCookies(), response -> {
            String body = response.getParsed();
            if (body == null) {
                errorCallback.onError(new NetworkError(101111, 403, "No membership list retrieved!"));
                return;
            }
            JSONArray memberships;
            try {
                JSONObject json = new JSONObject(body);
                memberships = json.getJSONArray("membership_collection");
            } catch (JSONException e) {
                e.printStackTrace();
                errorCallback.onError(new NetworkError(101112, 403, "Cannot parse membership list!"));
                return;
            }
            final int[] latch = {memberships.length()};
            if (latch[0] == 0) {
                successCallback.onResponse(modules);
                return;
            }
            for (int i = 0; i < memberships.length(); i++) {
                try {
                    JSONObject membership = memberships.getJSONObject(i);
                    String locationReference = membership.getString("locationReference");
                    String courseId = Regex.regex("/site/([^/]*)", locationReference);
                    Modules.Module kvvCourse = cacheKVVCourse().getKVVCourse(courseId);
                    if (kvvCourse != null) {
                        kvvCourse = kvvCourse.clone();
                        modules.addModule(kvvCourse);
                        if (--latch[0] == 0) successCallback.onResponse(modules);
                        continue;
                    }
                    get(String.format(Constants.KVV_SERVER_URL+"direct/site/%s.json?_validateSession=", courseId), mLogin.getLoginTokenKVV().getCookies(), response1 -> {
                        String body1 = response1.getParsed();
                        if (body1 == null) {
                            errorCallback.onError(new NetworkError(101113, 403, "No site retrieved!"));
                            return;
                        }
                        try {
                            JSONObject site = new JSONObject(body1);

                            String semester_string = site.getJSONObject("props").optString("term_eid", null);
                            Semester semester;
                            if (semester_string == null)
                                semester = null;
                            else
                                semester = new Semester(semester_string);
                            HashSet<String> lvNumbers = new HashSet<>();
                            String kvv_lvnumbers = site.getJSONObject("props").optString("kvv_lvnumbers", null);
                            if (kvv_lvnumbers != null)
                                for (MatchResult matchResult : Regex.allMatches("[0-9]+", kvv_lvnumbers)) {
                                    lvNumbers.add(matchResult.group());
                                }
                            String title = site.getString("entityTitle");
                            LinkedHashSet<Lecturer> lecturers = new LinkedHashSet<>();
                            String kvv_lecturers = site.getJSONObject("props").optString("kvv_lecturers", null);
                            if (kvv_lecturers != null) for (String lecturer : kvv_lecturers.split("#")) {
                                if (lecturer.length() > 2)
                                    lecturers.add(new Lecturer(lecturer));
                            }
                            String type = site.getJSONObject("props").optString("kvv_coursetype", "Projekt");
                            String description = site.optString("description", "");
                            description = String.valueOf(PartModules.fromHtml(description));
                            String id = site.getString("id");
                            Modules.Module module = modules.addModule(semester, lvNumbers, title, lecturers, type, description, id, Modules.TYPE_KVV);
                            cacheKVVCourse().setKVVCourse(courseId, module.clone());
                            if (--latch[0] == 0) successCallback.onResponse(modules);
                        } catch (JSONException e) {
                            log.e(new NetworkError(101114, 403, "Cannot parse site!"));
                            log.e("JSON:", body1);
                            e.printStackTrace();
                        } catch (NoSuchFieldException e) {
                            log.e(new NetworkError(101115, 403, "Cannot parse site!"));
                            e.printStackTrace();
                        }
                    }, error -> errorCallback.onError(new NetworkError(101116, error.networkResponse.statusCode, "Cannot get parse!")));
                } catch (JSONException e) {
                    log.e("ID:", i, "JSON:", memberships);
                    e.printStackTrace();
                    errorCallback.onError(new NetworkError(101117, 403, "Cannot parse membership list!"));
                    return;
                } catch (NoSuchFieldException e) {
                    log.e("ID:", i, "JSON:", memberships);
                    e.printStackTrace();
                    errorCallback.onError(new NetworkError(101118, 403, "Cannot parse membership list!"));
                    return;
                }
            }
        }, error -> errorCallback.onError(new NetworkError(101119, error.networkResponse.statusCode, "Cannot get membership list!")));
    }

    private void upgradeBB(final Modules modulesKVV, final NetworkCallback<Modules> callback, final NetworkErrorCallback errorCallback) {
        NetworkCallback<Modules> successCallback = (modules -> {
            try {
                cacheBBCourse().save(getContext());
            } catch (IOException e) {
                e.printStackTrace();
            }
            callback.onResponse(modules);
        });
        NetworkCallback<Modules> fastCallback = (modules -> {
            mListener.onModuleListPartiallyUpdated(modules);
            try {
                cacheBBCourse().save(getContext());
            } catch (IOException e) {
                e.printStackTrace();
            }
            mListener.onModuleListPartiallyUpdated(modules);
        });

        if (!mLogin.isInOnlineMode() || mLogin.getLoginTokenBB() == null) {
            errorCallback.onError(new NetworkError(101120, 500, "Currently running in offline mode!"));
            return;
        }
        if (!mLogin.getLoginTokenBB().isAvailable()) {
            callback.onResponse(modulesKVV);
            return;
        }
        get(String.format("https://lms.fu-berlin.de/learn/api/public/v1/users/%s/courses?fields=courseId,dataSourceId", mLogin.getLoginTokenBB().getId()), mLogin.getLoginTokenBB().getCookies(), response -> {
            String body = response.getParsed();
            if (body == null) {
                errorCallback.onError(new NetworkError(101121, 403, "No module list retrieved!"));
                return;
            }
            final JSONArray[] sites = new JSONArray[1];
            try {
                JSONObject json = new JSONObject(body);
                sites[0] = json.getJSONArray("results");
            } catch (JSONException e) {
                e.printStackTrace();
                errorCallback.onError(new NetworkError(101122, 403, "Cannot parse module list!"));
                return;
            }
            final int[] latch = {sites[0].length()};
            // fetching lecturers takes a lot of time, send success twice: first without lecturers
            final int[] latchNoLecturers = {sites[0].length()};
            if (sites[0].length() == 0) {
                successCallback.onResponse(modulesKVV);
                return;
            }
            for (int i = 0; i < sites[0].length(); i++) {
                try {
                    JSONObject site = sites[0].getJSONObject(i);
                    String courseId = site.getString("courseId");
                    if (cacheBBCourse().hasKVVCourseID(courseId)) {
                        if (--latchNoLecturers[0] == 0) fastCallback.onResponse(modulesKVV);
                        if (--latch[0] == 0) successCallback.onResponse(modulesKVV);
                        continue;
                    }
                    Modules.Module bbCourse = cacheBBCourse().getBBCourse(courseId);
                    if (bbCourse != null) {
                        bbCourse = bbCourse.clone();
                        modulesKVV.addModule(bbCourse);
                        if (--latchNoLecturers[0] == 0) fastCallback.onResponse(modulesKVV);
                        if (--latch[0] == 0) successCallback.onResponse(modulesKVV);
                        continue;
                    }
                    get(String.format("https://lms.fu-berlin.de/learn/api/v1/courses/%s?fields=name,courseId,description", courseId), mLogin.getLoginTokenBB().getCookies(), response1 -> {
                        String body1 = response1.getParsed();
                        if (body1 == null) {
                            errorCallback.onError(new NetworkError(101124, 403, "No course entry retrieved!"));
                            return;
                        }
                        try {
                            JSONObject json = new JSONObject(body1);
                            String name = json.getString("name");
                            String description = json.optString("description", null);
                            String type, lvNumber, semYear, semType;
                            Semester semester = null;
                            HashSet<String> lvNumberSet = new HashSet<>();
                            boolean found = false;
                            try {
                                Matcher match = Regex.match("[A-Za-z0-9]*_([A-Za-z0-9]*)_([A-Za-z0-9]*)_([0-9]{2,})([WS]+)", json.getString("courseId"));
                                type = match.group(1);
                                lvNumber = match.group(2);
                                semYear = match.group(3);
                                semType = match.group(4);
                                semester = new Semester(semType.equals("W") ? Semester.SEM_WS : Semester.SEM_SS, Integer.parseInt(semYear) % 100);
                                lvNumberSet.add(lvNumber);
                                for (Modules.Module module: modulesKVV) {
                                    if (module.lvNumber.contains(lvNumber)) {
                                        found = true;
                                        break;
                                    }
                                }
                            } catch (NoSuchFieldException | NumberFormatException e) {
                                log.e(e);
                                type = "Projekt";
                            }
                            if (!found) {
                                Modules.Module module = modulesKVV.addModule(semester, lvNumberSet, name, new LinkedHashSet<>(), type, description, courseId, Modules.TYPE_BB);
                                lecturer().getBBLecturers(courseId, success -> {
                                    module.setLecturers(success);
                                    cacheBBCourse().setBBCourse(courseId, module.clone());
                                    if (--latch[0] == 0) successCallback.onResponse(modulesKVV);
                                }, error -> {
                                    log.e(error);
                                    if (--latch[0] == 0) successCallback.onResponse(modulesKVV);
                                });
                                if (--latchNoLecturers[0] == 0) fastCallback.onResponse(modulesKVV);
                            } else {
                                cacheBBCourse().addKVVCourseID(courseId);
                                if (--latchNoLecturers[0] == 0) fastCallback.onResponse(modulesKVV);
                                if (--latch[0] == 0) successCallback.onResponse(modulesKVV);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            log.e(new NetworkError(101125, 403, "Cannot parse course entry!"));
                            if (--latchNoLecturers[0] == 0) fastCallback.onResponse(modulesKVV);
                            if (--latch[0] == 0) successCallback.onResponse(modulesKVV);
                        }
                    }, error -> errorCallback.onError(new NetworkError(101126, error.networkResponse.statusCode, "Cannot get module list!")));
                } catch (JSONException e) {
                    log.e(new NetworkError(101123, 403, "Cannot parse course entry"));
                    log.e("ID:", i, "JSON:", sites[0]);
                    e.printStackTrace();
                }
            }
        }, error -> errorCallback.onError(new NetworkError(101125, error.networkResponse.statusCode, "Cannot get module list!")));
    }



    private ModulesListLecturer lecturer() {
        if (mLecturer == null)
            mLecturer = new ModulesListLecturer(getContext(), mLogin);
        return mLecturer;
    }

    private CacheBBCourse cacheBBCourse() {
        if (mBBCache == null) {
            try {
                mBBCache = CacheBBCourse.load(getContext());
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        if (mBBCache == null)
            mBBCache = new CacheBBCourse();
        return mBBCache;
    }

    private CacheKVVCourse cacheKVVCourse() {
        if (mKVVCache == null) {
            try {
                mKVVCache = CacheKVVCourse.load(getContext());
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        if (mKVVCache == null)
            mKVVCache = new CacheKVVCourse();
        return mKVVCache;
    }
}
