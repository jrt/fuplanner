package de.sebse.fuplanner.services.canteen.types;

import de.sebse.fuplanner.tools.SortedList;

public class SortedListCanteen extends SortedList<Canteen, Integer, String> {
    @Override
    public int compare(Canteen o1, Canteen o2) {
        // TODO correct implementation
        return Integer.compare(o1.getId(), o2.getId());
    }

    @Override
    public boolean hasIdentifier(Canteen o1, Integer id) {
        return o1.getId() == id;
    }

    @Override
    public boolean hasFilter(Canteen o1, String filter) {
        return o1.getCity().equals(filter);
    }
}
