package de.sebse.fuplanner.services.kvv.types;

import de.sebse.fuplanner.tools.DateSortedList;

public class EventList extends DateSortedList<Event> {

    @Override
    public long getDateByItem(Event item) {
        return item.getEndDate();
    }

    @Override
    public boolean reversed() {
        return false;
    }
}
