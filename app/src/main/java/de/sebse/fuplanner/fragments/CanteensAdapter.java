package de.sebse.fuplanner.fragments;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ResourceCursorAdapter;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import de.sebse.fuplanner.R;
import de.sebse.fuplanner.fragments.CanteensFragment.OnCanteensFragmentInteractionListener;
import de.sebse.fuplanner.services.canteen.types.Canteen;
import de.sebse.fuplanner.services.canteen.types.Canteens;
import de.sebse.fuplanner.services.kvv.types.Resource;
import de.sebse.fuplanner.tools.ui.CanteensViewHolder;
import de.sebse.fuplanner.tools.ui.CustomViewHolder;
import de.sebse.fuplanner.tools.ui.StringViewHolder;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Canteen} and makes a call to the
 * specified {@link OnCanteensFragmentInteractionListener}.
 */
class CanteensAdapter extends RecyclerView.Adapter<CustomViewHolder> {

    private static final int TYPE_DELETE_BUTTON = 1;
    private static final int TYPE_ENTRY = 2;
    private Canteens mValues;
    private final OnCanteensFragmentInteractionListener mListener;
    private boolean mIsShowDeletion = false;
    private ArrayList<Integer> mDeleteCanteenIds = new ArrayList<>();
    private DeletionListener mDeletionListener;

    CanteensAdapter(OnCanteensFragmentInteractionListener listener) {
        mValues = null;
        mListener = listener;
    }

    public void setCanteens(Canteens canteens) {
        mValues = canteens;
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_DELETE_BUTTON) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_canteens_btn_delete, parent, false);
            return new StringViewHolder(view);
        }
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_canteens_items, parent, false);
        return new CanteensViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        if (viewType == TYPE_DELETE_BUTTON) {
            ((StringViewHolder) holder).mString.setOnClickListener(v -> endDeletion());
        } else {
            if (mIsShowDeletion) {
                position--;
            }
            if (mValues == null)
                return;
            Canteen canteen = mValues.get(position);
            ((CanteensViewHolder) holder).mTitle.setText(canteen.getName());
            ((CanteensViewHolder) holder).mSubLeft.setText(canteen.getAddress());
            ((CanteensViewHolder) holder).mSubRight.setText(canteen.getCity());
            ((CanteensViewHolder) holder).mActionIcon.setVisibility(mIsShowDeletion ? View.VISIBLE : View.GONE);
            if (mDeleteCanteenIds.contains(canteen.getId())) {
                ((CanteensViewHolder) holder).mActionIcon.setImageDrawable(ResourcesCompat.getDrawable(holder.mView.getResources(), R.drawable.ic_remove_circle, null));
            } else {
                ((CanteensViewHolder) holder).mActionIcon.setImageDrawable(ResourcesCompat.getDrawable(holder.mView.getResources(), R.drawable.ic_remove_circle_outline, null));
            }
            ((CanteensViewHolder) holder).mActionIcon.setOnClickListener(v -> {
                if (mDeleteCanteenIds.contains(canteen.getId())) {
                    // Prevent calling remove with index
                    //noinspection SuspiciousMethodCalls
                    mDeleteCanteenIds.remove((Object) canteen.getId());
                } else {
                    mDeleteCanteenIds.add(canteen.getId());
                }
                notifyDataSetChanged();
            });



            holder.mView.setOnClickListener(v -> {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onCanteensFragmentInteraction(canteen.getId());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (mValues != null) {
            return mIsShowDeletion ? mValues.size() + 1 : mValues.size();
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return mIsShowDeletion && position == 0 ? TYPE_DELETE_BUTTON : TYPE_ENTRY;
    }

    public void startDeletion(DeletionListener listener) {
        mDeletionListener = listener;
        if (mValues.size() > 0) {
            mDeleteCanteenIds.clear();
            mIsShowDeletion = true;
            notifyDataSetChanged();
        }
    }

    public void endDeletion() {
        int[] canteenIds = new int[mDeleteCanteenIds.size()];
        for (int i = 0; i < mDeleteCanteenIds.size(); i++) {
            canteenIds[i] = mDeleteCanteenIds.get(i);
        }
        mDeletionListener.onDeletionFinished(canteenIds);
        mDeleteCanteenIds.clear();
        mIsShowDeletion = false;
        notifyDataSetChanged();
    }

    public interface DeletionListener {
        void onDeletionFinished(int[] canteenIds);
    }
}
